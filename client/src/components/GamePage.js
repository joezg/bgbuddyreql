import React, { Component } from 'react';
import { graphql, createPaginationContainer } from 'react-relay';
import { Segment, Header, Dropdown } from 'semantic-ui-react';
import MatchCard from './MatchCard';
import ItemCards from './ItemCards';
import createRelayRootRenderer from '../createRelayRootRenderer';

class GamePage extends Component {
    render() {

        const ownershipOptions = [
            {
                text: "own",
                value: true
            },
            {
                text: "do not own",
                value: false
            }
        ]

        const wantToPlayOptions = [
            {
                text: "want to play",
                value: true
            },
            {
                text: "don't want to play",
                value: false
            }
        ]

        const items = this.props.data.game.matches.edges.map((edge) => {
            return edge.node;
        })

        return (
            <div>
                <Segment raised>
                    <Header size="large" content={this.props.data.game.name} />
                    <p>You <Dropdown inline options={ownershipOptions} value={this.props.data.game.owned} /> this game.</p>
                    <p>You <Dropdown inline options={wantToPlayOptions} value={this.props.data.game.wantToPlay} /> this game.</p>
                    <p>You played <strong>{this.props.data.game.matchCount} matches </strong> of this game.</p>
                    <Segment>
                        <Header size="medium" content="Matches" />
                        <ItemCards
                            items={items}
                            itemComponent={MatchCard}
                            onLoadMoreClick={() => this.increaseNumber()}
                            hasMore={this.props.data.game.matches.pageInfo.hasNextPage} />
                    </Segment>
                </Segment>
            </div>
        );
    }

    increaseNumber() {
        if (!this.props.relay.hasMore() || this.props.relay.isLoading()) {
            return;
        }

        this.props.relay.loadMore(12);
    }
};

const GamePageFragmentContainer = createPaginationContainer(GamePage, 
    graphql`
      fragment GamePage on User {
        game(id: $id){
            name
            owned
            wantToPlay
            matchCount
            matches(first: $count, after: $cursor)@connection(key: "GamePage_matches"){
                pageInfo{
                    hasNextPage 
                    endCursor
                }
                edges{
                    node{
                        id
                        ...MatchCard
                    }
                }
            }
        }        
      }
    `,
  {
    getFragmentVariables(prevVars, totalCount) {
      return {
        ...prevVars,
        count: totalCount,
      };
    },
    getVariables(props, {count, cursor}, { id }) {
      return {
        count,
        cursor,
        id
      };
    },
    query: graphql`
      query GamePagePaginationQuery($id: ID!, $count: Int!, $cursor: String) {
        viewer {
          ...GamePage
        }
      }
    `
  }
);

export default createRelayRootRenderer({
    query: 
        graphql`
            query GamePageQuery($id: ID!, $count: Int, $cursor: String) { 
                viewer {
                    ...GamePage
                }
            }
        `,
    variables: ({match})=>{
        return {
            id: match.params.id,
            count: 8,
            cursor: ""
        };
    },
    Component: GamePageFragmentContainer
});
          