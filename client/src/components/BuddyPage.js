import React, { Component } from 'react';
import { graphql, createPaginationContainer } from 'react-relay';
import createRelayRootRenderer from '../createRelayRootRenderer';
import { Segment, Header, Dropdown } from 'semantic-ui-react';
import MatchCard from './MatchCard';
import ItemCards from './ItemCards';

class BuddyPage extends Component {
    render() {
        const favouriteOptions = [
            {
                text: "is",
                value: true
            },
            {
                text: "is not",
                value: false
            }
        ]

        const items = this.props.data.buddy.matches.edges.map((edge) => {
            return edge.node;
        })

        return (
            <div>
                <Segment raised>
                    <Header size="large" content={this.props.data.buddy.name} />
                    <p>This <Dropdown inline options={favouriteOptions} value={this.props.data.buddy.like} /> your favourite buddy.</p>
                    <p>You played <strong>{this.props.data.buddy.matchCount} matches </strong> together.</p>
                    <Segment>
                        <Header size="medium" content="Matches" />
                        <ItemCards
                            items={items}
                            itemComponent={MatchCard}
                            onLoadMoreClick={() => this.increaseNumber()}
                            hasMore={this.props.data.buddy.matches.pageInfo.hasNextPage} />
                    </Segment>
                </Segment>
            </div>
        );
    }

    increaseNumber() {
        if (!this.props.relay.hasMore() || this.props.relay.isLoading()) {
            return;
        }

        this.props.relay.loadMore(12);
    }
};

const BuddyPageFragmentContainer = createPaginationContainer(BuddyPage, 
    graphql`
      fragment BuddyPage on User {
        buddy(id: $id){
            name
            like
            matchCount
            matches(first: $count, after: $cursor)@connection(key: "BuddyPage_matches"){
                pageInfo{
                    hasNextPage 
                    endCursor
                }
                edges{
                    node{
                        id
                        ...MatchCard
                    }
                }
            }
        }        
      }
    `,
  {
    getFragmentVariables(prevVars, totalCount) {
      return {
        ...prevVars,
        count: totalCount,
      };
    },
    getVariables(props, {count, cursor}, { id }) {
      return {
        count,
        cursor,
        id
      };
    },
    query: graphql`
      query BuddyPagePaginationQuery($id: ID!, $count: Int!, $cursor: String) {
        viewer {
          ...BuddyPage
        }
      }
    `
  }    
);

export default createRelayRootRenderer({
    query: 
        graphql`
            query BuddyPageQuery($id: ID!, $count: Int, $cursor: String) { 
                viewer {
                    ...BuddyPage
                }
            }
        `,
    variables: ({match})=>{
        return {
            id: match.params.id,
            count: 8,
            cursor: ""
        };
    },
    Component: BuddyPageFragmentContainer
});
