import React, { Component } from 'react';
import { graphql, createFragmentContainer } from 'react-relay';
import moment from 'moment';
import { Card, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom'

class GameCard extends Component {
  render() {
    const item = this.props.data;
    
    const playedNumberTimes = <span>played <strong>{item.matchCount}</strong> {item.matchCount === 1 ? ' time' : ' times' }</span>;
    
    return (
      <Card raised>
        <Card.Content>
          <Card.Header>
            {item.name}
          </Card.Header>
          <Card.Meta>
            {item.owned ? "Owned" : "Not owned"}
            {item.owned !== item.played ? ", but " : " and "}
            {item.played ? playedNumberTimes : "not played"}
          </Card.Meta>
          <Card.Description>
            {item.dateFirstPlayed ? <div>First time played: {moment(item.dateFirstPlayed).fromNow()}</div> : null }
            {item.dateLastPlayed ? <div>Last time played: {moment(item.dateLastPlayed).fromNow()}</div> : null }
            {item.wantToPlay && item.played ? <strong>I want to play this game again</strong> : null}
            {item.wantToPlay && !item.played ? <strong>I want to play this game</strong> : null}
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
          <Button negative floated="right" icon="delete" />
          <Link to={`/game/${item.id}`}><Button positive floated="right" icon="browser" /></Link>
        </Card.Content>
      </Card>
    );
  }
}

export default createFragmentContainer(GameCard, 
  graphql`
    fragment GameCard on Game {
        id
        name
        owned
        played
        wantToPlay
        dateFirstPlayed
        dateLastPlayed
        matchCount
    }
  `
);