import React, { Component } from 'react';
import { graphql, createPaginationContainer } from 'react-relay';
import createRelayRootRenderer from '../createRelayRootRenderer';
import ItemCards from './ItemCards';
import GameCard from './GameCard';

class Games extends Component {
  render() {
    const { data } = this.props;
    const items = data.games.edges.map((edge) => {
      return edge.node;
    })
    return (
      <ItemCards
        items={items}
        itemComponent={GameCard}
        onLoadMoreClick={() => this.increaseNumber()}
        hasMore={this.props.relay.hasMore()} />
    );
  }

  increaseNumber() {
    if (!this.props.relay.hasMore() || this.props.relay.isLoading()) {
      return;
    }

    this.props.relay.loadMore(12);
  }
}

const GamesPaginationContainer = createPaginationContainer(Games, 
  graphql`
    fragment Games on User {
      games(first: $count, after: $cursor)@connection(key: "Games_games"){
        pageInfo{
          hasNextPage 
          endCursor
        }
        edges{
          node{
            id
            ...GameCard
          }
        }
      }
    }
  `,
  {
    getFragmentVariables(prevVars, totalCount) {
      return {
        ...prevVars,
        count: totalCount,
      };
    },
    getVariables(props, {count, cursor}, fragmentVariables) {
      return {
        count,
        cursor
      };
    },
    query: graphql`
      query GamesPaginationQuery($count: Int!, $cursor: String) {
        viewer {
          ...Games
        }
      }
    `
  }
);

export default createRelayRootRenderer({
    query: 
        graphql`
          query GamesQuery($count: Int!, $cursor: String) { 
            viewer {
              ...Games
            }
          }
        `,
    variables: {
      count: 12,
      cursor:''
    },
    Component: GamesPaginationContainer
});