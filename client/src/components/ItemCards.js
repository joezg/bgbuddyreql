import React, { Component } from 'react';
import { Card, Button, Container } from 'semantic-ui-react';
import './ItemCards.css';

class ItemCards extends Component {
  render() {
    return (
      <Container>
        <Card.Group itemsPerRow={4} stackable>
          {this.props.items.map((item) => {
            return (
              <this.props.itemComponent key={item.id} data={item} parentProps={this.props.itemProps} onDelete={this.props.onDelete} />
            )
          })}
        </Card.Group>
        {this.props.hasMore ? <Button id="loadMore" onClick={this.props.onLoadMoreClick} fluid>Load more</Button> : null }
        <Button id="add" icon="plus" positive circular size="massive" onClick={this.props.onAddButonClick} />
      </Container>
    );
  }
}

export default ItemCards;