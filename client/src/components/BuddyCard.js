import React, { Component } from 'react';
import { graphql, createFragmentContainer } from 'react-relay';
import { Card, Button, Icon } from 'semantic-ui-react';
import './BuddyCard.css';
import { Link } from 'react-router-dom'

class BuddyCard extends Component {
  render() {
    const item = this.props.data;
    
    const playedNumberTimes = <span>played <strong>{item.matchCount}</strong> {item.matchCount === 1 ? ' time' : ' times' }</span>;
    
    return (
      <Card raised>
        <Card.Content>
          <Card.Header>
            {item.name}
            {item.like ? <Icon id="likeIcon" circular inverted color="red" size="small" floated="right" name='like outline' /> : null}
          </Card.Header>
          <Card.Meta>
            {playedNumberTimes}
          </Card.Meta>
          <Card.Description>
            
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
          <Button negative floated="right" icon="delete" />
          <Link to={`/buddy/${item.id}`}><Button positive floated="right" icon="browser" /></Link>
        </Card.Content>
      </Card>
    );
  }
}

export default createFragmentContainer(BuddyCard, 
  graphql`
    fragment BuddyCard on Buddy {
        id
        name
        like
        isUser
        matchCount
    }
  `
);
