import React, { Component } from 'react';
import { graphql, createFragmentContainer } from 'react-relay';
import { Card, Label, Popup } from 'semantic-ui-react';

class PlayerCard extends Component {
  render() {
    const player = this.props.data;
    return (
      <Card raised>
        <Card.Content>
          <Card.Header content={player.isAnonymous ? <em>"Anonymous"</em> : player.name} />
          <Card.Description>
            { player.winner ? 
              <Popup
                  trigger={<Label icon="trophy" corner="right" />}
                  position="top center"
                  content="winner"
              /> : null }
            { this.props.parentProps.isScored ? 
              <div>
                <p>
                  Rank: {player.rank}
                </p>
                <p>
                  Result: {player.result}
                </p>
              </div> : null }
          </Card.Description>
        </Card.Content>
      </Card>
    );
  }
}

export default createFragmentContainer(PlayerCard, 
  graphql`
    fragment PlayerCard on Player {
      id
      winner
      result
      rank
      ... on PlayerUser{
        name
      }
      ... on PlayerBuddy{
        name
      }
      ... on PlayerAnonymous{
        isAnonymous
      }
    }
  `
);

