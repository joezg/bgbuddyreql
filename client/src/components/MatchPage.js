import React, { Component } from 'react';
import { graphql, createFragmentContainer } from 'react-relay';
import createRelayRootRenderer from '../createRelayRootRenderer';
import moment from 'moment';
import { Segment, Header, Label } from 'semantic-ui-react';
import PlayerCard from './PlayerCard';
import ItemCards from './ItemCards';

class MatchPage extends Component {
    render() {
        const match = this.props.data.match;
        const formattedDate = moment(match.date).fromNow();
        const subheader = `${formattedDate} at ${match.location}`;
        const ribbonVissible = match.isNewGame || match.isGameOutOfDust; 
        return (
            <div>
                <Segment raised>
                    { match.isNewGame ? <Label color='orange' ribbon="right">New game</Label> : null }
                    { match.isGameOutOfDust ? <Label color='brown' ribbon="right">Out of dust</Label> : null }
                    <Header 
                        size="large" 
                        content={match.game} 
                        subheader={subheader}
                        style={{
                            "margin-top": ribbonVissible ? "-25px" : "0"
                        }} />
                    
                    <Segment>
                        <Header size="medium" content="Players" />
                        <ItemCards
                            items={match.players}
                            itemComponent={PlayerCard}
                            hasMore={false}
                            itemProps={{isScored: match.isScored}} />
                    </Segment>
                </Segment>
            </div>
        );
    }
};

const MatchPageFragmentContainer = createFragmentContainer(MatchPage,
    graphql`
      fragment MatchPage on User {
        match(id: $id){
            game
            date
            location
            isNewGame
            isGameOutOfDust
            isScored
            players{
                ...PlayerCard
            }
        }    
      }    
    `    
);

export default createRelayRootRenderer({
    query: 
        graphql`
            query MatchPageQuery($id: ID!) { 
                viewer {
                    ...MatchPage
                }
            }
        `,
    variables: ({match})=>{
        return {
            id: match.params.id
        };
    },
    Component: MatchPageFragmentContainer
});