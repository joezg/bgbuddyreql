import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'; 
import { Menu, Container } from 'semantic-ui-react';
import { Games, Home, Buddies, Matches, BuddyPage, GamePage, MatchPage } from '.';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <Container>
          <Menu inverted fixed="top" >
            <Link to="/">
              <Menu.Item content="bgBuddy" />
            </Link>
          </Menu>
          <Container id="mainContainer">
            <Route exact path="/" component={Home} />
            <Route exact path="/games" component={Games} />
            <Route exact path="/buddies" component={Buddies} />
            <Route exact path="/matches" component={Matches} />
            <Route exact path="/buddy/:id" component={BuddyPage} />
            <Route exact path="/game/:id" component={GamePage} />
            <Route exact path="/match/:id" component={MatchPage} />
          </Container>
        </Container>
      </Router>
    );
  }
}

export default App;