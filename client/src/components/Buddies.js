import React, { Component } from 'react';
import { graphql, createPaginationContainer } from 'react-relay';
import ItemCards from './ItemCards';
import BuddyCard from './BuddyCard';
import createRelayRootRenderer from '../createRelayRootRenderer';

class Buddies extends Component {
  render() {
    const items = this.props.data.buddies.edges.map((edge)=>{
        return edge.node;
    })
    return (
      <ItemCards 
        items={items} 
        itemComponent={BuddyCard} 
        onLoadMoreClick={() => this.increaseNumber()} 
        hasMore={this.props.relay.hasMore()} />
    );
  }

  increaseNumber() {
    if (!this.props.relay.hasMore() || this.props.relay.isLoading()) {
      return;
    }

    this.props.relay.loadMore(12);
  }
}

const BuddiesPaginationContainer = createPaginationContainer(Buddies, 
  graphql`
    fragment Buddies on User {
      buddies(first: $count, after: $cursor)@connection(key: "Buddies_buddies"){
        pageInfo{
          hasNextPage 
          endCursor
        }
        edges{
          node{
            id
            ...BuddyCard
          }
        }
      }
    }
  `,
  {
    getFragmentVariables(prevVars, totalCount) {
      return {
        ...prevVars,
        count: totalCount,
      };
    },
    getVariables(props, {count, cursor}, fragmentVariables) {
      return {
        count,
        cursor
      };
    },
    query: graphql`
      query BuddiesPaginationQuery($count: Int!, $cursor: String) {
        viewer {
          ...Buddies
        }
      }
    `
  }
);

export default createRelayRootRenderer({
    query: 
        graphql`
          query BuddiesQuery($count: Int!, $cursor: String) { 
            viewer {
              ...Buddies
            }
          }
        `,
    variables: {
      count: 12,
      cursor:''
    },
    Component: BuddiesPaginationContainer
});