import React, { Component } from 'react';
import { graphql, createPaginationContainer } from 'react-relay';
import createRelayRootRenderer from '../createRelayRootRenderer';
import ItemCards from './ItemCards';
import MatchCard from './MatchCard';
import { deleteMatch as deleteMatchMutation } from '../mutations/match.mutations';

class Matches extends Component {
  render() {
    const items = this.props.data.matches.edges.map((edge)=>{
        return edge.node;
    })
    return (
      <ItemCards 
        items={items} 
        itemComponent={MatchCard} 
        onLoadMoreClick={() => this.increaseNumber()} 
        hasMore={this.props.relay.hasMore()}
        onDelete={this.deleteHandler.bind(this)}/>
    );
  }

  deleteHandler(id){
    const afterMutation = ()=>{
      if (this.props.relay.hasMore()){
        this.props.relay.loadMore(1);    
      }
    }

    deleteMatchMutation(id, afterMutation);
  }

  increaseNumber() {
    if (!this.props.relay.hasMore() || this.props.relay.isLoading()) {
      return;
    }

    this.props.relay.loadMore(1);
  }
}

const MatchesPaginationContainer = createPaginationContainer(Matches, 
  graphql`
    fragment Matches on User {
      matches(first: $count, after: $cursor)@connection(key: "Matches_matches"){
        pageInfo{
          hasNextPage 
          endCursor
        }
        edges{
          node{
            id
            ...MatchCard
          }
        }
      }
    }
  `,
  {
    getFragmentVariables(prevVars, totalCount) {
      return {
        ...prevVars,
        count: totalCount,
      };
    },
    getVariables(props, {count, cursor}, fragmentVariables) {
      return {
        count,
        cursor
      };
    },
    query: graphql`
      query MatchesPaginationQuery($count: Int!, $cursor: String) {
        viewer {
          ...Matches
        }
      }
    `
  }
);

export default createRelayRootRenderer({
    query: 
        graphql`
          query MatchesQuery($count: Int!, $cursor: String) { 
            viewer {
              ...Matches
            }
          }
        `,
    variables: {
      count: 12,
      cursor:''
    },
    Component: MatchesPaginationContainer
});