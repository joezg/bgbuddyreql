import React, { Component } from 'react';
import { graphql, createFragmentContainer } from 'react-relay';
import moment from 'moment';
import { Card, List, Popup, Label, Button } from 'semantic-ui-react';
import './MatchCard.css';
import { Link } from 'react-router-dom'

class MatchCard extends Component {
  render() {
    const item = this.props.data;
    const subtitle = moment(item.date).fromNow() + ' at ' + item.location 
    
    return (
      <Card raised>
        <Card.Content>
          <Card.Header>
            { item.isNewGame ? <Label className="info" color='orange' tag>New game</Label> : null }
            { item.isGameOutOfDust ? <Label className="info" color='brown' tag>Out of dust</Label> : null }
            {item.game}
          </Card.Header>
          <Card.Meta>
            {subtitle}
          </Card.Meta>
          <Card.Description>
            <List>
              { item.players.map((player)=>{
                let nameComponent = player.isAnonymous ?
                    <List.Content as="em" content="Anonymous" /> :
                    <List.Content content={player.name} />;
                
                return (
                  <List.Item key={player.id}>
                      <List.Icon name='user' />
                      { nameComponent }
                      { player.winner ? 
                          <Popup
                            trigger={<List.Icon name='trophy' />}
                            position="right center"
                            content="winner"
                          /> : null }
                      
                  </List.Item>
                )
              })}
            </List>
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
          <Button negative floated="right" icon="delete" onClick={this.deleteMatch.bind(this)} />
          <Link to={`/match/${item.id}`}><Button positive floated="right" icon="browser" /></Link>
        </Card.Content>
      </Card>
    );
  }
  deleteMatch(){
    this.props.onDelete(this.props.data._id);
  }
}

export default createFragmentContainer(MatchCard, 
  graphql`
    fragment MatchCard on Match {
      id
      _id
      date
      game
      isNewGame
      isGameOutOfDust
      location
      players {
        id
        winner
        ... on PlayerUser{
          name
        }
        ... on PlayerBuddy{
          name
        }
        ... on PlayerAnonymous{
          isAnonymous
        }
      }
    }
  `
);

