import React, { Component } from 'react';
import { graphql, createFragmentContainer } from 'react-relay';
import createRelayRootRenderer from '../createRelayRootRenderer';
import { Link } from 'react-router-dom';

class Home extends Component {
  render() {
    const { data } = this.props;
    return (
      <div>
        Hello <span>{data.username}</span>
        <ul>
          <li><Link to="/matches">Matches</Link></li>
          <li><Link to="/buddies">Buddies</Link></li>
          <li><Link to="/games">Games</Link></li>
        </ul>
      </div>
    );
  }
}

const HomeFragmentContainer = createFragmentContainer(Home, 
  graphql`
    fragment Home on User{
      username
    }
  `  
);

export default createRelayRootRenderer({
    query: 
        graphql`
          query HomeQuery { 
            viewer {
              ...Home
            }
          }
        `,
    Component: HomeFragmentContainer
});