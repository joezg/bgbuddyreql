import React from 'react';
import { QueryRenderer } from 'react-relay';
import environment from './createRelayEnvironment';
import { Loader, Dimmer } from 'semantic-ui-react';

export default ({query, variables, Component})=>{
    return (propsParent)=>{
        let vars = variables;
        if (typeof variables === 'function'){
          vars = variables(propsParent);
        }
        return <QueryRenderer
            environment={environment}
            query={query()}
            variables={vars}
            
            render={({error, props}) => {
              if (error) {
                return <div>{error.message}</div>;
              } else if (props) {
                return <Component data={props.viewer} />;
              }
              
              return <Dimmer active >
                        <Loader size="big">Loading</Loader>
                    </Dimmer>;
            }}
          />
    }
}
