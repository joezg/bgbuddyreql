import { commitMutation, graphql } from 'react-relay';
import { ConnectionHandler } from 'relay-runtime';
import environment from '../createRelayEnvironment';

export const deleteMatch = (_id, afterMutation) => {
    commitMutation(
        environment,
        {
            mutation: graphql`
                mutation match_mutationsDeleteMutation($input: DeleteMatchInput){
                    deleteMatch(input: $input){
                        match{
                            id
                        }
                    }
                }
            `,
            variables: { input: {_id} },
            onCompleted: (response) => afterMutation && afterMutation(response),
            onError: err => console.error(err),
            optimisticResponse: ()=>{
                const response = {
                    deleteMatch: {
                        match: {
                            game: "deleting..."
                        }
                    }
                }
                return response;
            },
            updater: (store)=>{
                const match = store.getRootField('deleteMatch').getLinkedRecord('match');
                const deletedMatchId = match.getValue('id')
                const viewer = store.getRoot().getLinkedRecord('viewer');
                const conn = ConnectionHandler.getConnection(
                    viewer,
                    'Matches_matches'
                );
                ConnectionHandler.deleteNode(
                    conn,
                    deletedMatchId,
                );
            }
        },
    );
}