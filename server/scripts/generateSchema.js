require('babel-register');
const fs = require('fs');
const path = require('path');
const {graphql, introspectionQuery, printSchema} = require('graphql');

const schema = require('../graphql/graphql.schema').default;
const yourSchemaPath = path.join(__dirname, '../../client/schema');

// Save JSON of full schema introspection for Babel Relay Plugin to use
graphql(schema, introspectionQuery).then(result => {
  console.log("Writing shema.json...");
  fs.writeFileSync(
    `${yourSchemaPath}.json`,
    JSON.stringify(result, null, 2)
  );
  console.log("Done!");
});

// Save user readable type system shorthand of schema
console.log("Writing shema.graphql...");
fs.writeFileSync(
  `${yourSchemaPath}.graphql`,
  printSchema(schema)
);
console.log("Done!");