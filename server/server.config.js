import express from 'express';
import graphqlHTTP from 'express-graphql';
import GraphQLSchema from './graphql/graphql.schema'

const app = express();

app.use('/graphql', graphqlHTTP({ 
    schema: GraphQLSchema,
    graphiql: true,
    context: {
        userId: "58946eb110221ae437000003" //TODO get from session"
    }
}));

app.listen(3001, () => {
    console.log({ express: 'OK' })
});
