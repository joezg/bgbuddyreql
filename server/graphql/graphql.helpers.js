import moment from 'moment';
import { connectionArgs, connectionFromPromisedArray  } from 'graphql-relay';

export const dateFormattedResolver = function(dateInstance) {
    return moment(dateInstance).format();
}

export const dateIsoResolver = function(dateInstance) {
    return dateInstance ? moment(dateInstance).toISOString() : null;
}

export const dateRelativeResolver = function(dateInstance) {
    return moment(dateInstance).fromNow();
}

export const getUniqueIdResolver = function(modelName){
    return (source)=>{
        var compound = new Buffer(modelName + ':' + source._id);
        return compound.toString('base64');
    }
}

export const decodeUniqueId = function(id){
    const key64 = new Buffer(id, 'base64')
    const key = key64.toString();
    const [ modelName, _id ] = key.split(":");
    return { modelName, _id };
}

export const getIdNameFilterResolver = function(Model, nameFieldName = 'name') {
    return (source, args) => {
        const { nameIncludes, startsWith, ...rest } = args;

        if (rest.id) {
            return Model.findById(rest.id).lean().then((result) => {
                return [result];
            })
        } else {
            rest[nameFieldName] = { $in: [rest[nameFieldName]] };

            if (startsWith) {
                rest[nameFieldName].$in.push(new RegExp('^' + args.startsWith, 'i'));
            }

            if (nameIncludes) {
                rest[nameFieldName].$in.push(new RegExp(args.nameIncludes, 'i'));
            }

            const query = { userId: source._id, ...rest };

            return Model.find(query).lean();
        }
    }
}

export const getUserSubcollectionConnection = function(Model, ConnectionType, sort, userIdSourceField, idFilterField){
    userIdSourceField = userIdSourceField || "_id"; 
    sort = sort || {};
    return {
        type: ConnectionType,
        args: connectionArgs,
        resolve(source, args) {
            const filter = {};
            if (idFilterField){
                filter[idFilterField] = source._id
            }

            return connectionFromPromisedArray(
                Model.find({ userId: source[userIdSourceField], ...filter })
                    .sort(sort), args);
        }
    }
}