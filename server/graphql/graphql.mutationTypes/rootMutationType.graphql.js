import { GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLBoolean, GraphQLID} from 'graphql';
import { Game, Buddy, Match } from '../../mongo/mongo.models';
import { createModelBasicMutations } from './mutationHelpers.graphql';
import { GameType, BuddyType, MatchType } from '../graphql.types'

export const RootMutationType = new GraphQLObjectType({
    name: 'RootMutation',
    description: "The Root mutation",
    fields: {
        ...createModelBasicMutations(Game, GameType, {
            name: {
                type: new GraphQLNonNull(GraphQLString)
            },
            owned: {
                type: new GraphQLNonNull(GraphQLBoolean)
            }
        }),
        ...createModelBasicMutations(Buddy, BuddyType, {
            name: {
                type: new GraphQLNonNull(GraphQLString)
            },
            isUser: {
                type: GraphQLBoolean
            },
            like: {
                type: GraphQLBoolean
            }
        }),
        ...createModelBasicMutations(Match, MatchType, {
            date: {
                type: new GraphQLNonNull(GraphQLString)
            },
            gameId: {
                type: new GraphQLNonNull(GraphQLID)
            },
            isGameOutOfDust: {
                type: GraphQLBoolean
            },
            isNewGame: {
                type: GraphQLBoolean
            },
            isScored: {
                type: GraphQLBoolean
            },
            location: {
                type: new GraphQLNonNull(GraphQLString)
            },
        })
    }
});