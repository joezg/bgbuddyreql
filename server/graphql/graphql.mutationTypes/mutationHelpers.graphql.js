import { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLBoolean, GraphQLID, GraphQLNonNull } from 'graphql';

// add add and delete mutation for Model
export const createModelBasicMutations = function (Model, PayloadType, inputFields) {
    const modelName = Model.modelName;
    const smallCapsModelName = modelName.charAt(0).toLowerCase() + modelName.slice(1);

    const payloadFields = {
        [smallCapsModelName]: {
           type: PayloadType
        }
    };
    
    return {
        ['add' + modelName]: {
            type: new GraphQLObjectType({
                name: "Add" + modelName + "Payload",
                fields: payloadFields
            }),
            args: {
                input: {
                    type: new GraphQLInputObjectType({
                        name: "Add" + modelName + "Input",
                        fields: inputFields
                    })
                }
            },
            resolve(source, args, context) {
                const newInstance = new Model({ userId: context.userId, ...args.input })

                return newInstance.save().then((result) => {
                    return {
                        [smallCapsModelName]: result
                    }
                });
            }
        },
        ['delete' + modelName]: {
            type: new GraphQLObjectType({
                name: "Delete" + modelName + "Payload",
                fields: payloadFields
            }),
            args: {
                input: {
                    type: new GraphQLInputObjectType({
                        name: "Delete" + modelName + "Input",
                        fields: {
                            _id: {
                                type: new GraphQLNonNull(GraphQLID)
                            }
                        }
                    })
                }
            },
            resolve(source, args, context) {
                return Model.findByIdAndRemove(args.input._id).then((result) => {
                    return {
                        [smallCapsModelName]: result
                    }
                });
            }
        }
    }
}