import { GraphQLSchema } from 'graphql';
import { RootQueryType, PlayerAnonymousType, PlayerBuddyType, PlayerUserType, MatchType, BuddyType, GameType, UserType } from './graphql.types';
import { RootMutationType } from './graphql.mutationTypes'; 

const Schema = new GraphQLSchema({
    types: [ PlayerAnonymousType, PlayerBuddyType, PlayerUserType, MatchType, BuddyType, GameType, UserType ],
    query: RootQueryType,
    mutation: RootMutationType
});

export default Schema;