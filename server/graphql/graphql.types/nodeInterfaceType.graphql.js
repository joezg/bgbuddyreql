import { GraphQLInterfaceType, GraphQLObjectType, GraphQLBoolean, GraphQLID, GraphQLInt, GraphQLString, GraphQLNonNull } from 'graphql';
import { MatchType } from './matchType.graphql';
import { GameType } from './gameType.graphql';
import { BuddyType } from './buddyType.graphql';
import { UserType } from './userType.graphql';

export const NodeInterfaceType = new GraphQLInterfaceType({
    name: 'Node',
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLID),
        }
    },
    resolveType(source, args, context){
        switch (source.constructor.modelName) {
            case "Match":
                return MatchType;       
                break;
            case "Buddy":
                return BuddyType;       
                break;
            case "Game":
                return GameType;       
                break;
            case "User":
                return UserType;
                break;
            default:
                return null;
                break;
        }
    }
});