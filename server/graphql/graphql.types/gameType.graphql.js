import { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLBoolean, GraphQLID, GraphQLNonNull } from 'graphql';
import { dateIsoResolver, getUniqueIdResolver, getUserSubcollectionConnection } from '../graphql.helpers'
import { Match } from '../../mongo/mongo.models';
import { MatchConnectionType } from './matchType.graphql';
import { connectionDefinitions } from 'graphql-relay';
import { NodeInterfaceType } from './nodeInterfaceType.graphql';

export const GameType = new GraphQLObjectType({
    name: 'Game',
    interfaces: ()=> { return [ NodeInterfaceType ] },
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLID),
            resolve: getUniqueIdResolver('Game')
        },
        _id: {
            type: new GraphQLNonNull(GraphQLID)
        },
        name: {
            type: new GraphQLNonNull(GraphQLString),
        },
        owned: {
            type: GraphQLBoolean,
        },
        played: {
            type: GraphQLBoolean,
        },
        wantToPlay: {
            type: GraphQLBoolean,
        },
        dateFirstPlayed: {
            type: GraphQLString,
            resolve(source){
                return dateIsoResolver(source.dateFirstPlayed);
            }
        },
        dateLastPlayed: {
            type: GraphQLString,
            resolve(source){
                return dateIsoResolver(source.dateLastPlayed);
            }
        },
        matches: getUserSubcollectionConnection(Match, MatchConnectionType, {date: -1}, "userId", "gameId"),
        matchCount: {
            type: GraphQLInt,
            resolve(source){
                return Match.count({userId: source.userId, gameId: source._id});
            }
        },
    }
});

export const GameConnectionType = connectionDefinitions({nodeType: GameType}).connectionType;