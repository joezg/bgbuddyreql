import { GraphQLObjectType, GraphQLID, GraphQLNonNull } from 'graphql';
import { UserType } from './userType.graphql';
import { NodeInterfaceType } from './nodeInterfaceType.graphql'
import { User, Match, Buddy, Game } from '../../mongo/mongo.models';
import { decodeUniqueId } from '../graphql.helpers';

export const RootQueryType = new GraphQLObjectType({
    name: 'RootQuery',
    description: "The root query",
    fields: {
        viewer: {
            type: UserType,
            resolve(source, args, context) {
                return User.findById(context.userId).lean();
            }
        },
        node: {
            type: NodeInterfaceType,
            args: {
                id: {
                    type: new GraphQLNonNull(GraphQLID)
                }
            },
            resolve(source, args, context, info){
                const { modelName, _id } = decodeUniqueId(args.id);

                let Model = null;
                if (modelName == 'Match'){
                    Model = Match;
                } else if (modelName == 'Buddy'){
                    Model = Buddy;
                } else if (modelName == 'Game') {
                    Model = Game;
                } else if (modelName == 'User'){
                    Model = User;
                }

                return Model.findById(_id);
            }
        }
    }
})