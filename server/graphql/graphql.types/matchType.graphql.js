import { GraphQLObjectType, GraphQLString, GraphQLBoolean, GraphQLID, GraphQLNonNull, GraphQLList } from 'graphql';
import { dateRelativeResolver, dateIsoResolver, getUniqueIdResolver } from '../graphql.helpers';
import { PlayerInterfaceType } from './playerInterfaceType.graphql'; 
import { connectionDefinitions } from 'graphql-relay';
import { NodeInterfaceType } from './nodeInterfaceType.graphql';

export const MatchType = new GraphQLObjectType({
    name: 'Match',
    interfaces: ()=> { return [ NodeInterfaceType ] },
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLID),
            resolve: getUniqueIdResolver('Match')
        },
        _id: {
            type: new GraphQLNonNull(GraphQLID),
        },
        game: {
            type: new GraphQLNonNull(GraphQLString),
        },
        dateRelative: {
            type: new GraphQLNonNull(GraphQLString),
            resolve(source){
                return dateRelativeResolver(source.date);
            }
        },
        date: {
            type: new GraphQLNonNull(GraphQLString),
            resolve(source){
                return dateIsoResolver(source.date);
            }
        },
        gameId: {
            type: new GraphQLNonNull(GraphQLID),
        },
        isGameOutOfDust: {
            type: new GraphQLNonNull(GraphQLBoolean),
        },
        isNewGame: {
            type: new GraphQLNonNull(GraphQLBoolean),
        },
        isScored: {
            type: new GraphQLNonNull(GraphQLBoolean),
        },
        location: {
            type: new GraphQLNonNull(GraphQLString),
        },
        players: {
            type: new GraphQLList(PlayerInterfaceType),
        }
    }
});

export const MatchConnectionType = connectionDefinitions({nodeType: MatchType}).connectionType;