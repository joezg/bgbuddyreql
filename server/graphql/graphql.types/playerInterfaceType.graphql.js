import { GraphQLInterfaceType, GraphQLObjectType, GraphQLBoolean, GraphQLID, GraphQLInt, GraphQLString, GraphQLNonNull } from 'graphql';

export const PlayerInterfaceType = new GraphQLInterfaceType({
    name: 'Player',
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLID),
        },
        rank: {
            type: GraphQLInt,
        },
        result: {
            type: GraphQLInt,
        },
        winner: {
            type: GraphQLBoolean,
        },
    },
    resolveType(source){
        if (source.isUser){
            return PlayerUserType;
        } else if (source.isAnonymous){
            return PlayerAnonymousType;
        } 

        return PlayerBuddyType;
    }
});

export const PlayerAnonymousType = new GraphQLObjectType({
    name: 'PlayerAnonymous',
    interfaces: [ PlayerInterfaceType ],
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLID),
        },
        anonymousId: {
            type: GraphQLInt,
        },
        isAnonymous: {
            type: GraphQLBoolean,
        },
        rank: {
            type: GraphQLInt,
        },
        result: {
            type: GraphQLInt,
        },
        winner: {
            type: GraphQLBoolean,
        },
    }
});

export const PlayerBuddyType = new GraphQLObjectType({
    name: 'PlayerBuddy',
    interfaces: [ PlayerInterfaceType ],
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLID),
        },
        buddyId: {
            type: GraphQLID,
        },
        name: {
            type: GraphQLString,
        },
        rank: {
            type: GraphQLInt,
        },
        result: {
            type: GraphQLInt,
        },
        winner: {
            type: GraphQLBoolean,
        },
    }
});

export const PlayerUserType = new GraphQLObjectType({
    name: 'PlayerUser',
    interfaces: [ PlayerInterfaceType ],
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLID),
        },
        buddyId: {
            type: GraphQLID,
        },
        name: {
            type: GraphQLString,
        },
        isUser: {
            type: GraphQLBoolean,
        },
        rank: {
            type: GraphQLInt,
        },
        result: {
            type: GraphQLInt,
        },
        winner: {
            type: GraphQLBoolean,
        },
    }
});