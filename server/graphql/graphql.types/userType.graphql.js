import { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLBoolean, GraphQLID, GraphQLNonNull, GraphQLList } from 'graphql';
import { decodeUniqueId, dateRelativeResolver, dateFormattedResolver, getIdNameFilterResolver, getUserSubcollectionConnection, getUniqueIdResolver } from '../graphql.helpers'
import { Match, Buddy, Game } from '../../mongo/mongo.models';
import { MatchType, MatchConnectionType } from './matchType.graphql';
import { BuddyType, BuddyConnectionType } from './buddyType.graphql';
import { GameType, GameConnectionType } from './gameType.graphql';
import { NodeInterfaceType } from './nodeInterfaceType.graphql';

export const UserType = new GraphQLObjectType({
    name: 'User',
    interfaces: ()=> { return [ NodeInterfaceType ] },
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLID),
            resolve:getUniqueIdResolver('User'),
        },
        _id: {
            type: new GraphQLNonNull(GraphQLID),
        },
        username: {
            type: new GraphQLNonNull(GraphQLString),
        },
        email: {
            type: new GraphQLNonNull(GraphQLString),
        },
        sex: {
            type: new GraphQLNonNull(GraphQLString),
        },
        cookieDisclaimerDismissed: {
            type: new GraphQLNonNull(GraphQLBoolean),
        },
        isConfirmed: {
            type: new GraphQLNonNull(GraphQLBoolean),
        },
        memberSinceDate: {
            type: new GraphQLNonNull(GraphQLString),
            resolve(source){
                return dateFormattedResolver(source.memberSince);
            }
        },
        memberSince: {
            type: new GraphQLNonNull(GraphQLString),
            resolve(source){
                return dateRelativeResolver(source.memberSince);
            }
        },
        color: {
            type: new GraphQLNonNull(GraphQLString),
        },
        matches: getUserSubcollectionConnection(Match, MatchConnectionType, {date:-1}),
        buddies: getUserSubcollectionConnection(Buddy, BuddyConnectionType, {name: 1}),
        games: getUserSubcollectionConnection(Game, GameConnectionType, {name: 1}),
        buddy: {
            type: BuddyType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID)}
            },
            resolve(source, args){
                const { _id } = decodeUniqueId(args.id);
                return Buddy.findById(_id);
            }
        },
        game: {
            type: GameType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID)}
            },
            resolve(source, args){
                const { _id } = decodeUniqueId(args.id);
                return Game.findById(_id);
            }
        },
        match: {
            type: MatchType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID)}
            },
            resolve(source, args){
                const { _id } = decodeUniqueId(args.id);
                return Match.findById(_id);
            }
        },
        matchCount: {
            type: GraphQLInt,
            resolve(source){
                return Match.count({userId: source._id});
            }
        },
        matchesFiltered: {
            type: new GraphQLList(MatchType),
            args: {
                id:{ type: GraphQLID, },
                game:{ type: GraphQLString, },
                nameIncludes: {type: GraphQLString },
                startsWith: {type: GraphQLString },
            },
            resolve: getIdNameFilterResolver(Match, 'game'),
        },
        buddiesFiltered: {
            type: new GraphQLList(BuddyType),
            args: {
                id:{ type: GraphQLID, },
                name:{ type: GraphQLString, },
                nameIncludes: {type: GraphQLString },
                startsWith: {type: GraphQLString },
            },
            resolve: getIdNameFilterResolver(Buddy),
        },
        gamesFiltered: {
            type: new GraphQLList(GameType),
            args: {
                id:{ type: GraphQLID, },
                name:{ type: GraphQLString, },
                nameIncludes: {type: GraphQLString },
                startsWith: {type: GraphQLString },
            },
            resolve: getIdNameFilterResolver(Game),
        },
    }
});