import { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLBoolean, GraphQLID, GraphQLNonNull } from 'graphql';
import { getUniqueIdResolver, getUserSubcollectionConnection } from '../graphql.helpers'
import { Match } from '../../mongo/mongo.models';
import { MatchConnectionType } from './matchType.graphql';
import { connectionDefinitions } from 'graphql-relay';
import { NodeInterfaceType } from './nodeInterfaceType.graphql';

export const BuddyType = new GraphQLObjectType({
    name: 'Buddy',
    interfaces: ()=> { return [ NodeInterfaceType ] },
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLID),
            resolve: getUniqueIdResolver('Buddy'),
        },
        _id: {
            type: new GraphQLNonNull(GraphQLID),
        },
        name: {
            type: new GraphQLNonNull(GraphQLString),
        },
        like: {
            type: new GraphQLNonNull(GraphQLBoolean),
        },
        isUser: {
            type: new GraphQLNonNull(GraphQLBoolean),
        },
        matches: getUserSubcollectionConnection(Match, MatchConnectionType, {date: -1}, "userId", "players.buddyId"),
        matchCount: {
            type: GraphQLInt,
            resolve(source){
                return Match.count({userId: source.userId, "players.buddyId": source._id});
            }
        },
    }
});

export const BuddyConnectionType = connectionDefinitions({nodeType: BuddyType}).connectionType;