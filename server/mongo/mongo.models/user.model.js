import mongoose from 'mongoose';

const userSchema = mongoose.Schema({
    username: String,
    email: String,
    sex: String,
    cookieDisclaimerDismissed: Boolean,
    isConfirmed: Boolean,
    memberSince: Date,
    color: String
}, { collection: 'user' });

export default mongoose.model('User', userSchema);