export {default as Buddy} from './buddy.model'
export {default as Game} from './game.model'
export {default as Match} from './match.model'
export {default as User} from './user.model'