import mongoose from 'mongoose';

const matchSchema = mongoose.Schema({
    userId: mongoose.Schema.Types.ObjectId,
    date: Date,
    game: String,
    gameId: mongoose.Schema.Types.ObjectId,
    isGameOutOfDust: Boolean,
    isNewGame: Boolean,
    isScored: Boolean,
    location: String,
    players: [{
        buddyId: mongoose.Schema.Types.ObjectId,
        isUser: Boolean,
        isAnonymous: Boolean,
        anonymousId: Number,
        name: String,
        rank: Number,
        result: Number,
        winner: Boolean
    }]
}, { collection: 'user.match' });

export default mongoose.model('Match', matchSchema);