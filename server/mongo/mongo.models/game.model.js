import mongoose from 'mongoose';

const gameSchema = mongoose.Schema({
    userId: mongoose.Schema.Types.ObjectId,
    name: String,
    owned: {type: Boolean, default: false},
    played: {type: Boolean, default: false}, 
    wantToPlay: {type: Boolean, default: false},
    dateFirstPlayed: Date,
    dateLastPlayed: Date,
}, { collection: 'user.game' });

export default mongoose.model('Game', gameSchema);