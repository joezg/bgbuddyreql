import mongoose from 'mongoose';

const buddySchema = mongoose.Schema({
    userId: mongoose.Schema.Types.ObjectId,
    name: String,
    isUser: {type: Boolean, default: false},
    like: {type: Boolean, default: false},    
}, { collection: 'user.buddy' });

export default mongoose.model('Buddy', buddySchema);